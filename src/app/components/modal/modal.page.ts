import { Component, OnInit, Input } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
  @Input() pelicula: any[];
  url: String = "https://image.tmdb.org/t/p/w220_and_h330_face/";
  constructor(public modalController: ModalController, navParams: NavParams) {   }

  ngOnInit() {  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
