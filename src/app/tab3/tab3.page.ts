import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../components/modal/modal.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  categorias: string[] = ['Inicio', 'En cartelera', 'Mejor votadas', 'Proximamente'];
  peliculas: any[] =[];
  url: String = "https://image.tmdb.org/t/p/w220_and_h330_face/";
  constructor(public modalController: ModalController, private http: HttpClient) {}
  ngOnInit(): void {
    this.getPelisInicio();
  }

  cambiarCategoria(event){
    this.peliculas=[];
    if(event.detail.value == 'Inicio'){
      this.getPelisInicio();
    }
    if(event.detail.value == 'En cartelera'){
      this.getPelisCartelera();
    }
    if(event.detail.value == 'Mejor votadas'){
      this.getPelisTop();
    }
    if(event.detail.value == 'Proximamente'){
      this.getPelisProx();
    }
  }

  getPelisInicio(){
    return new Promise(res => {
      this.http.get('https://api.themoviedb.org/3/movie/popular?api_key=f37484aaff5a79b57e82d3bc9c2ecbea&language=en-US&page=1' )
      .subscribe((res : any) => {
        this.peliculas = res.results;
        console.log(this.peliculas);
      });
    });
  }

  getPelisTop(){
    return new Promise(res => {
      this.http.get('https://api.themoviedb.org/3/movie/top_rated?api_key=f37484aaff5a79b57e82d3bc9c2ecbea&language=en-US&page=1' )
      .subscribe((res : any) => {
        this.peliculas = res.results;
        console.log(this.peliculas);
      });
    });
  }

  getPelisCartelera(){
    return new Promise(res => {
        this.http.get('https://api.themoviedb.org/3/movie/now_playing?api_key=f37484aaff5a79b57e82d3bc9c2ecbea&language=en-US&page=1' )
        .subscribe((res : any) => {
           this.peliculas = res.results;
           console.log(this.peliculas);
        });
    });
  }

  getPelisProx(){
    return new Promise(res => {
      this.http.get('https://api.themoviedb.org/3/movie/upcoming?api_key=f37484aaff5a79b57e82d3bc9c2ecbea&language=en-US&page=1' )
      .subscribe((res : any) => {
        this.peliculas = res.results;
        console.log(this.peliculas);
      });
    });
  }

  async showModal(pelicula: any[]) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'pelicula': pelicula,
      }
    });
    return await modal.present();
  }
}