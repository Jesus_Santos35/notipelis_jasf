import { Component, ViewChild } from '@angular/core';
import { Article } from '../interfaces/interface';
import { NoticiasService } from '../services/noticias.service';
import { IonSegment } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild(IonSegment, { static: true }) segment: IonSegment;

  categorias: string[] = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  noticias: Article[] = [];
  pelicula: any[] =[];

  constructor(private http: HttpClient, private noticiasService: NoticiasService) { }

  ngOnInit(): void {
    this.segment.value = this.categorias[0];
    this.cargarNoticias(this.segment.value);
    this.getPeliculas();
  }

  cambiarCategoria(event){
    this.noticias=[];
    this.cargarNoticias(event.detail.value);
  }

  cargarNoticias(categoria: string) {
    this.noticiasService.obtenerTitularesPorCategoria(categoria)
      .subscribe(res => {
        console.log('resultados:', res);
        this.noticias.push(...res.articles);
      });
  }
  getPeliculas(){
    return new Promise(res => {
        this.http.get('https://api.themoviedb.org/3/movie/now_playing?api_key=f37484aaff5a79b57e82d3bc9c2ecbea&language=en-US&page=1' )
        .subscribe((res : any) => {
          // console.log(res);
           this.pelicula= res.results[2].original_title;
           console.log(this.pelicula);
        });
    });
  }
}
